# Docstring:
"""
LSC Noise Budget Guardian

This guardian simply uses the LSC-NOISE bank to send white noise from an
always running 

Authors: A. Mullavey, A. Effler
Date: 2020-11-10
Contact: adam.mullavey@ligo.org
"""

#------------------------------------------------------------------------------
# Imports
from guardian import GuardState, GuardStateDecorator
import isclib.matrices as matrix
import time
import gpstime
import csv
from os import path

#------------------------------------------------------------------------------
 
nominal = "IDLE"

#------------------------------------------------------------------------------
# Variables

fieldnames = ["Bkgrnd Start","Bkgrnd Stop","Inj Start","Inj Stop"]

# top path TODO: decide on directory structure
pathname = "/data/l1nb/meas_times/"

bkgrnd_fname = "background_times"

#------------------------------------------------------------------------------
# Functions

def set_fbank(gain=0.0,filts=[],tramp=0.1):

    ezca.switch("LSC-NOISE","FMALL","OFF")
    time.sleep(0.1)

    ezca.write("LSC-NOISE_TRAMP",tramp)

    if len(filts) > 0:
        for filt in filts:
            ezca.switch("LSC-NOISE","FM{}".format(filt),"ON")

    ezca.switch("LSC-NOISE","INPUT","OUTPUT","ON")

    ezca.write("LSC-NOISE_GAIN",gain)

def reset_fbank():
    set_fbank()


#------------------------------------------------------------------------------
# Guardian Decorators



#------------------------------------------------------------------------------
# State Generators

# MICH, PRCL and SRCL states are very similar, hence this common state generator
def make_inj_state(dof,gain=1.0,filts=[]):
    class INJ_FOR_DOF(GuardState):
        request = True

        def main(self):

            self.dof = dof

            # Self times 
            #self.meas_times = {}

            ### Check background
            if not path.exists(pathname + bkgrnd_fname + ".csv"):
                return "TAKE_BACKGROUND"
        
            with open(pathname + bkgrnd_fname + ".csv","r") as csvfile:
                reader = csv.DictReader(csvfile)
                list = []
                for row in reader:
                    list.append(dict(row))
                self.meas_times = list[-1]

            # If backgound is older than 10 minutes (?), take it again. TODO: How old can the background be?
            if gpstime.gpsnow()-float(self.meas_times["Bkgrnd Stop"]) > 3600:
                log("Last background too old, taking new background.")
                return "TAKE_BACKGROUND"
            elif gpstime.gpsnow()-float(self.meas_times["Bkgrnd Stop"]) < 0:
                log('Background stop time is in the future, moving to fault state')
                return "BACKGROUND_FAULT"
            else:
                log("Background taken within last hour, moving on with {} injection".format(dof))
            ###

            # File details
            filename = self.dof.lower()+"_inj_times"
            self.filename = pathname + filename + ".csv"

            # If file doesn't exist, create it.
            if not path.exists(self.filename):
                with open(self.filename,"w+") as csvfile:
                    writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
                    writer.writeheader()

            # Ensure matrix elements are zeroed
            matrix.lsc_input.TRAMP = 0
            time.sleep(0.1)
            matrix.lsc_input.zero(col='NOISE')
            time.sleep(0.3)
            matrix.lsc_input.load()

            # Set the filter bank, TODO: decide on filter settings
            set_fbank(gain,filts,tramp=0.1)
            time.sleep(0.3)

            # Time of measurement and ramp, TODO: set measurement time
            self.t_ramp = 3.0
            self.t_measurement = 64.0

            # Some flags
            self.record_start = True
            self.record_stop = True
            self.ramp_down = True
            self.write_to_csv = True

            # Ramp up matrix element, effectively starting the injection
            matrix.lsc_input.TRAMP = self.t_ramp
            matrix.lsc_input[self.dof,"NOISE"] = 1.0
            time.sleep(0.1)
            matrix.lsc_input.load()


        def run(self):

            # Wait for matrix to stop ramping
            if matrix.lsc_input.is_ramping(self.dof,"NOISE"):
                return

            # Store the start time
            if self.record_start:
                self.timer["meas"] = self.t_measurement
                self.meas_times["Inj Start"] = gpstime.gpsnow()
                self.record_start = False

            
            if not self.timer["meas"]:
                notify("{} Injection Running".format(self.dof))
                return

            if self.record_stop:
                log("Stopping injection, recording stop time")
                self.meas_times["Inj Stop"] = gpstime.gpsnow()
                self.record_stop = False

            if self.ramp_down:
                log("Ramping down matrix element")     
                matrix.lsc_input[self.dof,'NOISE'] = 0
                time.sleep(0.1)
                matrix.lsc_input.load()
                self.ramp_down = False

            if self.write_to_csv:
                with open(self.filename,"a") as csvfile:
                    writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
                    writer.writerow(self.meas_times)
                self.write_to_csv = False

            notify("{} Injection Complete".format(self.dof))

            return True

    return INJ_FOR_DOF

#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

class IDLE(GuardState):
    #goto = True
    request = True
    index = 5
    
    # Main class method
    def main(self):

        # Set a timer
        self.timer["wait"] = 2

    # Run class method.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Timer has expired so proceed.
            return True

class BACKGROUND_FAULT(GuardState):
    request = False
    index = 2

    def run(self):

        ### Check background
        if not path.exists(pathname + bkgrnd_fname + ".csv"):
            return "TAKE_BACKGROUND"

        with open(pathname + bkgrnd_fname + ".csv","r") as csvfile:
            reader = csv.DictReader(csvfile)
            list = []
            for row in reader:
                list.append(dict(row))
            self.meas_times = list[-1]

        # If backgound is older than 10 minutes (?), take it again. TODO: How old can the background be?
        if gpstime.gpsnow() - float(self.meas_times["Bkgrnd Stop"]) < 0:
            notify('Background stop time is in the future, fix the backround times file')
            return      

        return True


class RESET_NOISE_INJ(GuardState):
    goto = True
    request = False
    index = 1

    def main(self):

        t_ramp = 3.0
        matrix.lsc_input.TRAMP = t_ramp
        time.sleep(1.0)
        matrix.lsc_input.zero(col='NOISE')
        time.sleep(0.3)
        matrix.lsc_input.load()

        self.timer["wait"] = t_ramp

        self.reset_fbank = True

    def run(self):

        if not self.timer["wait"]:
            return

        if self.reset_fbank:
            reset_fbank()
            self.reset_fbank = False

        return True


class TAKE_BACKGROUND(GuardState):
    request = False
    index = 10

    def main(self):
        # File details
        self.filename = pathname + bkgrnd_fname + ".csv"

        # If file doesn't exist, create it.
        if not path.exists(self.filename):
            with open(self.filename,"w+") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=fieldnames[0:2])
                writer.writeheader()

        # Self times 
        self.meas_times = {}


        # Time of measurement and ramp, TODO: set measurement time
        self.t_measurement = 64.0

        # Flags
        self.record_start = True
        self.record_stop = True
        self.write_to_csv = True

    def run(self):

        # Store the start time
        if self.record_start:
            self.timer["meas"] = self.t_measurement
            self.meas_times["Bkgrnd Start"] = gpstime.gpsnow()
            self.record_start = False

        if not self.timer["meas"]:
            notify("Taking background data.")
            return

        if self.record_stop:
            log("Finished taking background data, recording stop time")
            self.meas_times["Bkgrnd Stop"] = gpstime.gpsnow()
            self.record_stop = False

        if self.write_to_csv:
            with open(self.filename,"a") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=fieldnames[0:2])
                writer.writerow(self.meas_times)
            self.write_to_csv = False

        return True


INJ_FOR_MICH = make_inj_state("MICH",gain=200.0,filts=[1])
INJ_FOR_MICH.index = 20


INJ_FOR_PRCL = make_inj_state("PRCL",gain=1.0,filts=[])
INJ_FOR_PRCL.index = 30


INJ_FOR_SRCL = make_inj_state("SRCL",gain=35.0,filts=[3])
INJ_FOR_SRCL.index = 40


edges = [
    ("INIT", "IDLE"),
    ("RESET_NOISE_INJ","IDLE"),
    ("BACKGROUND_FAULT","IDLE"),
    ("IDLE", "INJ_FOR_MICH"),
    ("TAKE_BACKGROUND","INJ_FOR_MICH"),
    ("IDLE", "INJ_FOR_PRCL"),
    ("TAKE_BACKGROUND","INJ_FOR_PRCL"),
    ("IDLE", "INJ_FOR_SRCL"),
    ("TAKE_BACKGROUND","INJ_FOR_SRCL"),
    ("IDLE","TAKE_BACKGROUND",2)
] 


    
